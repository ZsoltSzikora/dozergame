package dozergame;

import java.util.Collection;

import org.dozer.DozerConverter;

// original idea: http://stackoverflow.com/a/1931455/498697
@SuppressWarnings("rawtypes")
public class CollectionToLongSizeConverter extends DozerConverter<Collection, Long> {

	public CollectionToLongSizeConverter() {
		super(Collection.class, Long.class);
	}

	@Override
	public Long convertTo(Collection source, Long destination) {
		return (null == source) ? 0L : (long) source.size();
	}

	@Override
	public Collection convertFrom(Long source, Collection destination) {
		throw new IllegalStateException("Unknown value!");
	}
}
