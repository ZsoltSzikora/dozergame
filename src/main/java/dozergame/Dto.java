package dozergame;

public class Dto {
	private long id;
	private String name;
	private long friendsCount;

	public Dto() {
		super();
	}

	public Dto(long id, String name, long friendsCount) {
		super();
		this.id = id;
		this.name = name;
		this.friendsCount = friendsCount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFriendsCount() {
		return friendsCount;
	}

	public void setFriendsCount(long friendsCount) {
		this.friendsCount = friendsCount;
	}

	@Override
	public String toString() {
		return "Dto [id=" + id + ", name=" + name + ", friendsCount=" + friendsCount + "]";
	}

}
