package dozergame;

import java.util.Arrays;
import java.util.HashSet;

import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

public class App {

	private static Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

	public static void main(String[] args) {

		Domain domain = new Domain(1, "name", new HashSet<String>(Arrays.asList(new String[] { "egyik", "masik" })));
		System.out.println("original: " + domain.toString());

		Dto dto = mapper.map(domain, Dto.class);
		System.out.println("converted: " + dto.toString());

		Domain domAgain = mapper.map(domain, Domain.class);
		System.out.println("converted again: " + domAgain.toString());
	}
}
