package dozergame;

import java.util.HashSet;
import java.util.Set;

public class Domain {
	private long id;
	private String name;
	private Set<String> friends = new HashSet<String>();

	public Domain() {
		super();
	}

	public Domain(long id, String name, Set<String> friends) {
		this.id = id;
		this.name = name;
		this.friends = friends;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getFriends() {
		return friends;
	}

	public void addFriend(String friend) {
		friends.add(friend);
	}

	public void removeFriend(String friend) {
		friends.remove(friend);
	}

	public long getFriendsCount() {
		return friends.size();
	}

	@Override
	public String toString() {
		return "Domain [id=" + id + ", name=" + name + ", friends=" + friends + "]";
	}

}
